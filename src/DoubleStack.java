import java.util.LinkedList;

public class DoubleStack {

   // Tegu pole plagiaadiga, koodi baasiks oli eelmisel aastal tehtud ja esitatud töö. Vabandan segaduse pärast ja et ei märkanud et mul on endiselt sees liigsed operandid.
   //https://bitbucket.org/viusti/kt3/ -privaatne repo
   //Võin lisada vaatamisõigused aga ei tea mis emailiga on seotud teie bitbucketi konto.


   private LinkedList<Double> doubleStack = new LinkedList();

   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack cloneStack = new DoubleStack();
      for (int i = doubleStack.size() - 1; i > -1; i--) {
         cloneStack.push(doubleStack.get(i));
      }
      return cloneStack;
   }

   public boolean stEmpty() {
      return doubleStack.isEmpty();
   }

   public void push (double a) {
      doubleStack.push(a);
   }

   public double pop() {
      if (stEmpty()){
         throw new RuntimeException("Can't 'pop' stack, stack is empty.");
      } else {
         return doubleStack.pop();
      }
   } // pop

   public void op (String s) {
      if (doubleStack.size() < 2) {
         throw new RuntimeException(String.format("Not enough numbers to perform operation: %s", s));
      }
      Double top = pop();
      Double secondTop = pop();
      if (s.equals("/") && top == 0){
         throw new RuntimeException("Can't divide by 0!");
      }
      switch (s) {
         case "+" : push((top + secondTop)); break;
         case "-" : push((secondTop - top)); break;
         case "*" : push((top * secondTop)); break;
         case "/" : push((secondTop / top)); break;
         default : throw new RuntimeException(String.format("Unsupported operator: %s", s));
      }
   }

   public double tos() {
      if (stEmpty()){
         throw new RuntimeException("Can't 'tos' stack, stack is empty!");
      } else {
         return doubleStack.get(0);
      }
   }

   @Override
   public boolean equals (Object o) {
      if (!(o instanceof DoubleStack)) {
         return false;
      }
      DoubleStack cast = (DoubleStack) o;
      if (cast.doubleStack.size() != doubleStack.size()) {
         return false;
      }
      for (int i = 0; i <= cast.doubleStack.size() - 1; i++) {
         if (!doubleStack.get(i).equals(cast.doubleStack.get(i))) {
            return false;
         }
      }
      return true;
   }

   @Override
   public String toString() {
      StringBuilder answer = new StringBuilder();
      if (doubleStack.size() == 0){
         return "";
      }
      for (int i = doubleStack.size() - 1; i > -1; i--) {
         answer.append(doubleStack.get(i));
      }
      return answer.toString();
   }

   public static double interpret (String pol) {
      DoubleStack interpretStack = new DoubleStack();
      String noWhitespace = pol.replaceAll("\t", "").trim();
      if (noWhitespace.isEmpty()){
         throw new RuntimeException("Empty expression!");
      }
      for (String element: noWhitespace.split(" ")) {
         try {
            interpretStack.push(Double.parseDouble(element));
         } catch (NumberFormatException e) {
            if (element.equals("+") || element.equals("-") || element.equals("/")|| element.equals("*")) {
               if (interpretStack.doubleStack.size() < 2) {
                  throw new RuntimeException(String.format("Cannot perform %1Ss in expression %2$s", element, pol));
               } else {
                  interpretStack.op(element);
               }
            } else if (!element.isEmpty()){
               throw new RuntimeException(String.format("Illegal symbol: %1$s. In expression: %2$s", element, pol));
            }
         }
      }
      if (interpretStack.doubleStack.size() > 1) {
         throw new RuntimeException(String.format("Too many numbers in expression: %s", pol));
      }
      return interpretStack.tos();
   }
}
